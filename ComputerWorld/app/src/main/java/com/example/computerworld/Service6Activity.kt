package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service6Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service6)

        //----------------------------------------------------
        val actionBar = supportActionBar
        actionBar!!.title = "Ремонт и заправка картриджей"
        actionBar.setDisplayHomeAsUpEnabled(true)

        //-----------------------------------------------------
        val arrayAdapter: ArrayAdapter<*>

        val service6 = arrayOf(

            "1. ЗАПРАВКА ЛАЗЕРНОГО КАРТРИДЖА СТАНДАРТ БЕЗ МАТЕРИАЛА — 280Р.",
            "2. ЗАПРАВКА ЛАЗЕРНОГО КАРТРИДЖА НЕ СТАНДАРТ БЕЗ МАТЕРИАЛА — ОТ 280Р.",
            "3. ЗАПРАВКА ЛАЗЕРНОГО КАРТРИДЖА СТАНДАРТ С МАТЕРИАЛОМ — 500Р.",
            "4. ЗАПРАВКА СТРУЙНОГО КАРТРИДЖА СТАНДАРТ С МАТЕРИАЛОМ — 300Р.",
            "5. ЗАПРАВКА СТРУЙНОГО КАРТРИДЖА УВЕЛИЧЕННОГО ОБЪЁМА С МАТЕРИАЛОМ — 500Р.",
            "6. ЗАПРАВКА СТРУЙНОГО КАРТРИДЖА УМЕНЬШЕННОГО ОБЪЁМА С МАТЕРИАЛОМ — 200Р.",
            "7. ПРОФИЛАКТИКА ЛАЗЕРНОГО КАРТРИДЖА СТАНДАРТ — 280Р.",
            "8. СРОЧНАЯ ЗАПРАВКА ЛАЗЕРНОГО КАРТРИДЖА (ДОПОЛНИТЕЛЬНО К СТОИМОСТИ) — 200Р."

            )

        var mListView = findViewById<ListView>(R.id.service6List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, service6)

        mListView.adapter = arrayAdapter
    }
}