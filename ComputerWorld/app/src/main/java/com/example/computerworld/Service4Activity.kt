package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service4Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service4)

        //----------------------------------------------------
        val actionBar = supportActionBar
        actionBar!!.title = "Ремонт оргтехники"
        actionBar.setDisplayHomeAsUpEnabled(true)

        //-----------------------------------------------------
        val arrayAdapter: ArrayAdapter<*>

        val service4 = arrayOf(

            "1. ДИАГНОСТИКА ОРГТЕХНИКИ НЕ СТАНДАРТ (МФУ/ПРИНТЕР/КОПИР) — 600Р.",
            "2. ДИАГНОСТИКА ОРГТЕХНИКИ СТАНДАРТ (МФУ/ПРИНТЕР/КОПИР) — 400Р.",
            "3. ПРОФИЛАКТИКА (МФУ/ПРИНТЕРА/КОПИРА) — ОТ 1000Р.",
            "4. ЗАМЕНА РОЛИКА ПОДАЧИ БУМАГИ БЕЗ МАТЕРИАЛА — ОТ 1000Р.",
            "5. ЗАМЕНА СТЕКЛА СКАНЕРА/МФУ БЕЗ МАТЕРИАЛА — 800Р.",
            "6. ЗАМЕНА ТЕРМОПЛЕНКИ БЕЗ МАТЕРИАЛА — ОТ 1 200Р.",
            "7. ИЗВЛЕЧЕНИЕ ИНОРОДНЫХ ПРЕДМЕТОВ ИЗ МФУ/ПРИНТЕРА/КОПИРА — 1000Р.",
            "8. ОЧИСТКА ОТСТОЙНИКА ПЕЧАТАЮЩЕЙ ГОЛОВКИ ПРИНТЕРА/МФУ — 500Р.",
            "9. ПРОМЫВКА ПЕЧАТАЮЩЕЙ ГОЛОВКИ СТРУЙНОГО ПРИНТЕРА/МФУ — ОТ 800Р.",
            "10. ПРОФИЛАКТИКА — ОТ 500Р.",
            "11. ПРОШИВКА ПРИНТЕРА — ОТ 1 200Р.",
            "12. РЕМОНТ БЛОКА ПИТАНИЯ (МФУ/ПРИНТЕР/КОПИР) — ОТ 750Р.",
            "13. РЕМОНТ ЛАЗЕРНОГО ПРИНТЕРА (БЕЗ СТОИМОСТИ ЗАПЧАСТЕЙ) — ОТ 1000Р.",
            "14. РЕМОНТ МЕХАНИКИ УСТРОЙСТВА (МФУ/ПРИНТЕР/КОПИР) БЕЗ МАТЕРИАЛА — ОТ 800Р.",
            "15. ЗАМЕНА БЛОКА ЛАЗЕРА, БЛОКА СКАНЕРА (МФУ/ПРИНТЕР/КОПИР) БЕЗ МАТЕРИАЛА — 1000Р.",
            "16. РЕМОНТ ТЕРМОЗАКРЕПИТЕЛЬНОГО УЗЛА БЕЗ МАТЕРИАЛА — ОТ 900Р.",
            "17. ЗАМЕНА ТОРМОЗНОЙ ПЛОЩАДКИ С МАТЕРИАЛОМ — ОТ 400Р.",
            "18. НАЦЕНКА ЗА СРОЧНОСТЬ РАБОТ ПО РЕМОНТУ ОРГТЕХНИКИ — 100%."

        )

        var mListView = findViewById<ListView>(R.id.service4List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, service4)

        mListView.adapter = arrayAdapter
    }
}