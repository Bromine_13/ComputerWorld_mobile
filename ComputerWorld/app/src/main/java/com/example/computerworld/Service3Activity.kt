package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service3Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service3)

        //----------------------------------------------------
        val actionBar = supportActionBar
        actionBar!!.title = "Работа с программным обеспечением"
        actionBar.setDisplayHomeAsUpEnabled(true)

        //-----------------------------------------------------
        val arrayAdapter: ArrayAdapter<*>

        val service3 = arrayOf(

            "1. АДМИНИСТРИРОВАНИЕ СЕРВЕРНОГО ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ(ПОЧАСОВАЯ ОПЛАТА) — ОТ 1500Р.",
            "2. ПЕРЕНОС ИНФОРМАЦИИ — ОТ 500Р.",
            "3. РАБОТА С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ ПРЕДОСТАВЛЕННЫМ ЗАКАЗЧИКОМ ПОЧАСОВАЯ — ОТ 1 200Р.",
            "4. ПРОФИЛАКТИЧЕСКИЕ РАБОТЫ С ПО (ОПТИМИЗАЦИЯ) — 1000Р.",
            "5. ВОССТАНОВЛЕНИЕ ИНФОРМАЦИИ С HDD — ОТ 1 000Р.",
            "6. ВОССТАНОВЛЕНИЕ ОПЕРАЦИОННОЙ СИСТЕМЫ — 800Р.",
            "7. КЛОНИРОВАНИЕ ЖЕСТКОГО ДИСКА — 800Р.",
            "8. УСТАНОВКА ДРАЙВЕРОВ УСТРОЙСТВ (ВСЕХ) — 700Р.",
            "9. УСТАНОВКА КОДЕКОВ — 250Р.",
            "10. УСТАНОВКА ОС (WIN 7, WIN 8, LINUX) ПРОИЗВОДИТСЯ ПРИ НАЛИЧИИ ЛИЦЕНЗИОННОГО КЛЮЧА У КЛИЕНТА — 800Р.",
            "11. УДАЛЕНИЕ БАНЕРА — 700Р.",
            "12. УДАЛЕНИЕ ВИРУСОВ — 1000Р.",
            "13. УСТАНОВКА АНТИВИРУСА AVAST (1ГОД) — 700Р.",
            "14. УСТАНОВКА АНТИВИРУСА КАСПЕРСКОГО (1ГОД) БЕЗ СТОИМОСТИ АНТИВИРУСА — 400Р.",
            "15. УСТАНОВКА АНТИВИРУСА КАСПЕРСКОГО IS (1ГОД) БЕЗ СТОИМОСТИ АНТИВИРУСА — 400Р.",
            "16. УСТАНОВКА ДРАЙВЕРА — 250Р.",
            "17. НАЦЕНКА ЗА СРОЧНОСТЬ РАБОТ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ — 100%."

            )

        var mListView = findViewById<ListView>(R.id.service3List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, service3)

        mListView.adapter = arrayAdapter
    }
}