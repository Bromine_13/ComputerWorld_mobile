package com.example.computerworld

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val actionBar = supportActionBar
        actionBar!!.title = "Главное меню"

        val btnService: Button = findViewById (R.id.btnService)

        btnService.setOnClickListener{
            val intent = Intent(this, ServiceActivity::class.java)
            startActivity(intent)
        }

        val btnContact: Button = findViewById (R.id.btnContact)

        btnContact.setOnClickListener{
            val intent = Intent(this, ContactActivity::class.java)
            startActivity(intent)
        }
    }
}

