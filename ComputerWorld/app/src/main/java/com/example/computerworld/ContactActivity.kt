package com.example.computerworld

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ContactActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)

        val actionBar = supportActionBar
        actionBar!!.title = "Контакты"
        actionBar.setDisplayHomeAsUpEnabled(true)

    }
}