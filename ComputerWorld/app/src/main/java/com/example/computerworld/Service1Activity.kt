package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service1)

        //----------------------------------------------------
        val actionBar = supportActionBar
        actionBar!!.title = "Ремонт системного блока"
        actionBar.setDisplayHomeAsUpEnabled(true)

        //-----------------------------------------------------
        val arrayAdapter: ArrayAdapter<*>

        val service1 = arrayOf(

            "1. ЗАМЕНА БАТАРЕЙКИ BIOS (С УЧЕТОМ СТОИМОСТИ БАТАРЕЙКИ) — 250Р.",
            "2. ЗАМЕНА БЛОКА ПИТАНИЯ (БЕЗ СТОИМОСТИ БЛОКА ПИТАНИЯ) — ОТ 350Р.",
            "3. ЗАМЕНА ВИДЕОКАРТЫ (БЕЗ СТОИМОСТИ ВИДЕОКАРТЫ) — 350Р.",
            "4. ЗАМЕНА ЖЕСТКОГО ДИСКА (БЕЗ СТОИМОСТИ ЖЕСТКОГО ДИСКА) — 350Р.",
            "5. ЗАМЕНА КУЛЕРА (БЕЗ СТОИМОСТИ КУЛЕРА) — ОТ 500Р.",
            "6. ЗАМЕНА МАТЕРИНСКОЙ ПЛАТЫ (БЕЗ СТОИМОСТИ МАТЕРИНСКОЙ ПЛАТЫ) — ОТ 1000Р.",
            "7. ЗАМЕНА CD/DVD ПРИВОДА (БЕЗ СТОИМОСТИ ПРИВОДА) — 250Р.",
            "8. ЗАМЕНА ПРОВОДА ПИТАНИЯ 6PIN/4 PIN (БЕЗ СТОИМОСТИ ПРОВОДА) — 100Р.",
            "9. ОБНОВЛЕНИЕ BIOS МАТЕРИНСКОЙ ПЛАТЫ — 500Р.",
            "10. РЕМОНТ МАТЕРИНСКОЙ ПЛАТЫ (БЕЗ СТОИМОСТИ КОМПЛЕКТУЮЩИХ И З/ЧАСТЕЙ) — ОТ 1 000Р.",
            "11. УСТАНОВКА ОЗУ (БЕЗ СТОИМОСТИ ОЗУ) — 250Р.",
            "12. ПРОФИЛАКТИКА И ТЕХНИЧЕСКОЕ ОБСЛУЖИВАНИЕ СИСТЕМНОГО БЛОКА (С ЗАМЕНОЙ ТЕРМОПАСТЫ) — 1500Р.",
            "13. НАЦЕНКА ЗА СРОЧНОСТЬ РАБОТ ПО РЕМОНТУ СИСТЕМНОГО БЛОКА — 100%.",

        )

        var mListView = findViewById<ListView>(R.id.service1List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, service1)

        mListView.adapter = arrayAdapter
    }
}