package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service2Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service2)

        //----------------------------------------------------
        val actionBar = supportActionBar
        actionBar!!.title = "Ремонт ноутбука"
        actionBar.setDisplayHomeAsUpEnabled(true)

        //-----------------------------------------------------
        val arrayAdapter: ArrayAdapter<*>

        val service2 = arrayOf(

            "1. ДИАГНОСТИКА (ОПРЕДЕЛЕНИЕ ОСНОВНОЙ НЕИСПРАВНОСТИ) — 500Р.",
            "2. ДИАГНОСТИКА (ОПРЕДЕЛЕНИЕ ОСНОВНОЙ НЕИСПРАВНОСТИ) — 500Р.",
            "3. ДИАГНОСТИКА (ПОЛНАЯ ДИАГНОСТИКА УСТРОЙСТВА С ПОДГОТОВКОЙ ДИАГНОСТИЧЕСКОЙ КАРТЫ ) — 1000Р.",
            "4. ДИАГНОСТИКА (ПОЛНАЯ ДИАГНОСТИКА УСТРОЙСТВА С ПОДГОТОВКОЙ ДИАГНОСТИЧЕСКОЙ КАРТЫ ) — 1000Р.",
            "5. РАЗБОРКА/СБОРКА НОУТБУКА — 700Р.",
            "6. ЗАМЕНА КЛАВИАТУРЫ НОУТБУКА (БЕЗ СТОИМОСТИ КЛАВИАТУРЫ) — ОТ 500Р.",
            "7. ЗАМЕНА КУЛЕРА (БЕЗ СТОИМОСТИ КУЛЕРА) — 1500Р.",
            "8. ЗАМЕНА КУЛЕРА (БЕЗ СТОИМОСТИ КУЛЕРА) — 1500Р.",
            "9. ЗАМЕНА ЖЕСТКОГО ДИСКА (БЕЗ СТОИМОСТИ ЖЕСТКОГО ДИСКА) — ОТ 300Р.",
            "10. ЗАМЕНА CD/DVD ПРИВОДА (БЕЗ СТОИМОСТИ ПРИВОДА) — ОТ 300Р.",
            "11. КЛОНИРОВАНИЕ ЖЕСТКОГО ДИСКА — 1000Р.",
            "12. ПАЙКА РАЗЪЁМОВ — ОТ 1 000Р.",
            "13. РЕМОНТ МАТЕРИНСКОЙ ПЛАТЫ НОУТБУКА (БЕЗ СТОИМОСТИ КОМПЛЕКТУЮЩИХ И З/ЧАСТЕЙ) — ОТ 2 000Р.",
            "14. УСТАНОВКА ОЗУ (БЕЗ СТОИМОСТИ ОЗУ) — ОТ 300Р.",
            "15. ПРОФИЛАКТИКА И ТЕХНИЧЕСКОЕ ОБСЛУЖИВАНИЕ НОУТБУКА (С ЗАМЕНОЙ ТЕРМОПАСТЫ) — ОТ 1 500Р.",
            "16. НАЦЕНКА ЗА СРОЧНОСТЬ РАБОТ ПО РЕМОНТУ НОУТБУКА — 100%.",

            )

        var mListView = findViewById<ListView>(R.id.service2List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, service2)

        mListView.adapter = arrayAdapter
    }
}