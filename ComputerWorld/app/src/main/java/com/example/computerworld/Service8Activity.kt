package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service8Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service8)

        //----------------------------------------------------
        val actionBar = supportActionBar
        actionBar!!.title = "Дополнительные услуги сервисного центра"
        actionBar.setDisplayHomeAsUpEnabled(true)

        //-----------------------------------------------------
        val arrayAdapter: ArrayAdapter<*>

        val service8 = arrayOf(

            "1. ВЫЕЗД ТЕХНИЧЕСКОГО СПЕЦИАЛИСТА ПО ГОРОДУ СРОЧНЫЙ — 1000Р.",
            "2. ВЫЕЗД ТЕХНИЧЕСКОГО СПЕЦИАЛИСТА ПО РАЙОНУ СРОЧНЫЙ — ОТ 1000Р.",
            "3. ВЫЕЗД ТЕХНИЧЕСКОГО СПЕЦИАЛИСТА ПО ГОРОДУ — 500Р.",
            "4. ВЫЕЗД ТЕХНИЧЕСКОГО СПЕЦИАЛИСТА ПО РАЙОНУ — ОТ 500Р.",
            "5. ДОСТАВКА ПО ГОРОДУ (ЛЕГКОВОЙ А/М) — 300Р.",
            "6. ДОСТАВКА ПО РАЙОНУ (ЛЕГКОВОЙ А/М) — ОТ 300Р.",
            "7. ПЕЧАТЬ СТАНДАРТ 1 ЛИСТ — 10Р.",
            "8. ПОЧАСОВАЯ РАБОТА — ОТ 500Р.",
            "9. ЭКСПЕРТИЗА ТЕХНИЧЕСКОГО СОСТОЯНИЯ ОРГТЕХНИКИ 1 ЕДИНИЦА ТЕХНИКИ — 500Р.",
            "10. ЭКСПЕРТИЗА ТЕХНИЧЕСКОГО СОСТОЯНИЯ ОРГТЕХНИКИ БОЛЕЕ 1ОЙ ЕДИНИЦЫ ТЕХНИКИ — 300Р.",
            "11. СРОЧНАЯ ЭКСПЕРТИЗА ТЕХНИЧЕСКОГО СОСТОЯНИЯ ОРГТЕХНИКИ 1 ЕДИНИЦА ТЕХНИКИ — 750Р."

        )

        var mListView = findViewById<ListView>(R.id.service8List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, service8)

        mListView.adapter = arrayAdapter
    }
}