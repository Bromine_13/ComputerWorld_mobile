package com.example.computerworld

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ServiceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)

        //----------------------------------------------------
        val actionBar = supportActionBar
        actionBar!!.title = "Услуги сервисного центра"
        actionBar.setDisplayHomeAsUpEnabled(true)

        //----------------------------------------------------
        val button: Button = findViewById (R.id.button)

        button.setOnClickListener{
            val intent = Intent(this, Service1Activity::class.java)
            startActivity(intent)
        }

        //----------------------------------------------------
        val button2: Button = findViewById (R.id.button2)

        button2.setOnClickListener{
            val intent = Intent(this, Service2Activity::class.java)
            startActivity(intent)
        }

        //----------------------------------------------------
        val button3: Button = findViewById (R.id.button3)

        button3.setOnClickListener{
            val intent = Intent(this, Service3Activity::class.java)
            startActivity(intent)
        }

        //----------------------------------------------------
        val button4: Button = findViewById (R.id.button4)

        button4.setOnClickListener{
            val intent = Intent(this, Service4Activity::class.java)
            startActivity(intent)
        }

        //----------------------------------------------------
        val button5: Button = findViewById (R.id.button5)

        button5.setOnClickListener{
            val intent = Intent(this, Service5Activity::class.java)
            startActivity(intent)
        }

        //----------------------------------------------------
        val button6: Button = findViewById (R.id.button6)

        button6.setOnClickListener{
            val intent = Intent(this, Service6Activity::class.java)
            startActivity(intent)
        }

        //----------------------------------------------------
        val button7: Button = findViewById (R.id.button7)

        button7.setOnClickListener{
            val intent = Intent(this, Service7Activity::class.java)
            startActivity(intent)
        }

        //----------------------------------------------------
        val button8: Button = findViewById (R.id.button8)

        button8.setOnClickListener{
            val intent = Intent(this, Service8Activity::class.java)
            startActivity(intent)
        }
    }
}