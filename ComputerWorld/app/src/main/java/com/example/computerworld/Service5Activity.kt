package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service5Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service5)

        //----------------------------------------------------
        val actionBar = supportActionBar
        actionBar!!.title = "Настройка сетевого оборудования"
        actionBar.setDisplayHomeAsUpEnabled(true)

        //-----------------------------------------------------
        val arrayAdapter: ArrayAdapter<*>

        val service5 = arrayOf(

            "1. НАСТРОЙКА ИНТЕРНЕТА ПРЯМОГО ПОДКЛЮЧЕНИЯ — 300Р.",
            "2. НАСТРОЙКА РОУТЕРА/МАРШРУТИЗАТОРА (WIFI) — 500Р.",
            "3. ОБЖАТИЕ СЕТЕВОГО ПРОВОДА — 50Р.",
            "4. ПОДКЛЮЧЕНИЕ ПК К ЛОКАЛЬНОЙ СЕТИ БЕЗ ПРОКЛАДКИ КАБЕЛЯ — 500Р.",
            "5. ПОДКЛЮЧЕНИЕ ПРОФЕССИОНАЛЬНОГО СЕТЕВОГО ПРИНТЕРА — 2500Р.",
            "6. ПОДКЛЮЧЕНИЕ СЕТЕВОГО ПРИНТЕРА (1 ПК) — 500Р.",
            "7. УСТАНОВКА СЕРВЕРНОГО ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ — 2500Р.",
            "8. ПОЧАСОВАЯ РАБОТА С СЕТЕВЫМ ОБОРУДОВАНИЕМ — ОТ 500Р.",
            "9. НАЦЕНКА ЗА СРОЧНОСТЬ РАБОТ С СЕТЕВЫМ ОБОРУДОВАНИЕМ — 50%."

        )

        var mListView = findViewById<ListView>(R.id.service5List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, service5)

        mListView.adapter = arrayAdapter
    }
}