package com.example.computerworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class Service7Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service7)

        //----------------------------------------------------
        val actionBar = supportActionBar
        actionBar!!.title = "Установка оборудования"
        actionBar.setDisplayHomeAsUpEnabled(true)

        //-----------------------------------------------------
        val arrayAdapter: ArrayAdapter<*>

        val service7 = arrayOf(

            "1. УСТАНОВКА ОБОРУДОВАНИЯ БЕЗ ПО — 100Р.",
            "2. УСТАНОВКА ОБОРУДОВАНИЯ С ПО — 300Р.",
            "3. УСТАНОВКА РОУТЕРА/МАРШРУТИЗАТОРА (WIFI) — 500Р.",
            "4. УСТАНОВКА ПРИНТЕРА/МФУ/КОПИРА СТАНДАРТ — 300Р.",
            "5. УСТАНОВКА ПРИНТЕРА/МФУ/КОПИРА ПРОФЕССИОНАЛЬНОГО — 2500Р.",
            "6. УСТАНОВКА РАБОЧЕЙ СТАНЦИИ — 300Р."

        )

        var mListView = findViewById<ListView>(R.id.service7List)

        arrayAdapter = ArrayAdapter(this,

            android.R.layout.simple_list_item_1, service7)

        mListView.adapter = arrayAdapter
    }
}